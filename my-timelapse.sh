#!/bin/bash

DATE=$(date +"%Y-%m-%d_%H%M")
TIME=30000
TL=2000

if [ -d $DATE ]; then
  if [ -L $DATE ]; then
    rm $DATE
  else
    rmdir $DATE
  fi
else
  mkdir $DATE
fi

if [ $# == 2 ]; then
  TIME=$1
  TL=$2
fi

raspistill -t $TIME -tl $TL -vf -o ./$DATE/timelapse_%06d.jpg
ls ./$DATE/*.jpg > ./$DATE/stills.txt
mencoder -nosound -ovc lavc -lavcopts vcodec=mpeg4:aspect=16/9:vbitrate=8000000 -vf scale=1920:1080 -o $DATE/timelapse.avi -mf type=jpeg:fps=10 mf://@$DATE/stills.txt
